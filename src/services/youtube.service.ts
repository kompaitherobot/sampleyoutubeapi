import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

 
@Injectable()
 
export class YoutubeService {
     googleToken = "AIzaSyAH4Okw4KFCoq7sz0S4MvdtLmXGwLAaiYM";
     maxResults = 10;
     url = 'https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=';
     constructor(public http: HttpClient) {}
     public getVideos(query:any){
        return this.http.get(this.url+query+'&type=video&order=viewCount&maxResults='+this.maxResults+'&key='+this.googleToken);
         
     }
 
}